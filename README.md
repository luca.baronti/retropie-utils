# Retropie Utils

Utility scripts for the [Retropie](https://retropie.org.uk/) system meant to automate common functions. No installation is required, but you have to specify your RetroPie system local IP in the configuration file (see below).

## Scripts included
The following scripts are included:
- **SendRetropieRoms.sh** send all the roms in the current directory to the correct roms directories in the RetroPie system. The matching is made by file extension, so a measure of arbitrarity has been used. For instance all *.zip files are assumed to be MAME roms. The list is also partial, albeit it includes the most common systems (nes, snes, gba, etc..).
- **BackupRetropieRomsAndBios.sh** copy the *roms* and *BIOS* directories from the RetroPie system to the current directory (be sure to have enough space in the current system). It does NOT perform a backup of external save files (e.g. nintendo DS DraStic save files are located in /opt/retropie/configs/nds/drastic/backup/ thust they need to be copied manually)
- **RestoreRetropieRomsAndBiosBackup.sh** copy the content of the *roms* and *BIOS* directories present in the current directory to the RetroPie system. The content present in the RetroPie system with the same name of the local content will be replaced, extra files *should* be preserved.

In any case, the current directory can be replaced by a target directory with the relative field in the configuration file.
## Configuration File
The file **retropie_info.conf** contains some configuration info used in the scripts. Fields are:
- **ip** the IP address (in the local subnet) of your RetroPie system. This field **is mandatory** and you have to fill it with your IP used.
- **user** the username of your RetroPie system. It is usually set as "pi" by default.
- **directory** this is the target directory used by the scripts. This is useful if you want to keep the scripts in a directory and (e.g.) have another directory to move the ROMs from. 

The last line of the configuration file must be a single newline.