#!/bin/bash

#####
# Makes here a backup of the bios and roms directory (you may need quite a lot of space)
typeset -A config # init array
config=( # set default values in config array
    [user]="pi"
    [ip]=""
    [directory]="."
)
while read line
do
    if echo $line | grep -F = &>/dev/null
    then
        varname=$(echo "$line" | cut -d '=' -f 1)
        config[$varname]=$(echo "$line" | cut -d '=' -f 2-)
    fi
done < retropie_info.conf

scp -r ${config[directory]}/BIOS/* ${config[user]}@${config[ip]}:~/RetroPie/BIOS/
scp -r ${config[directory]}/roms/* ${config[user]}@${config[ip]}:~/RetroPie/roms/
