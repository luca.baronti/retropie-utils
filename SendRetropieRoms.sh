#!/bin/bash
typeset -A config # init array
config=( # set default values in config array
    [user]="pi"
    [ip]=""
    [directory]="."
)
while read line
do
    if echo $line | grep -F = &>/dev/null
    then
        varname=$(echo "$line" | cut -d '=' -f 1)
        config[$varname]=$(echo "$line" | cut -d '=' -f 2-)
    fi
done < retropie_info.conf

# Super Nintendo
compgen -G ${config[directory]}/*.nes > /dev/null && scp ${config[directory]}/*.nes ${config[user]}@${config[ip]}:~/RetroPie/roms/nes/
compgen -G ${config[directory]}/*.fds > /dev/null && scp ${config[directory]}/*.fds ${config[user]}@${config[ip]}:~/RetroPie/roms/nes/
# Super Nintendo
compgen -G ${config[directory]}/*.sfc > /dev/null && scp ${config[directory]}/*.sfc ${config[user]}@${config[ip]}:~/RetroPie/roms/snes/
compgen -G ${config[directory]}/*.smc > /dev/null && scp ${config[directory]}/*.smc ${config[user]}@${config[ip]}:~/RetroPie/roms/snes/
compgen -G ${config[directory]}/*.swc > /dev/null && scp ${config[directory]}/*.swc ${config[user]}@${config[ip]}:~/RetroPie/roms/snes/
compgen -G ${config[directory]}/*.mgd > /dev/null && scp ${config[directory]}/*.mgd ${config[user]}@${config[ip]}:~/RetroPie/roms/snes/
# Nintendo DS
compgen -G ${config[directory]}/*.nds > /dev/null && scp ${config[directory]}/*.nds ${config[user]}@${config[ip]}:~/RetroPie/roms/nds/
# GameBoy
compgen -G ${config[directory]}/*.gb > /dev/null && scp ${config[directory]}/*.gb ${config[user]}@${config[ip]}:~/RetroPie/roms/gb/
# GameBoy Color
compgen -G ${config[directory]}/*.gbc > /dev/null && scp ${config[directory]}/*.gbc ${config[user]}@${config[ip]}:~/RetroPie/roms/gbc/
# GameBoy Advance
compgen -G ${config[directory]}/*.gba > /dev/null && scp ${config[directory]}/*.gba ${config[user]}@${config[ip]}:~/RetroPie/roms/gba/
# Amiga
compgen -G ${config[directory]}/*.lha > /dev/null && scp ${config[directory]}/*.lha ${config[user]}@${config[ip]}:~/RetroPie/roms/amiga/
# Sega Master System
compgen -G ${config[directory]}/*.sms > /dev/null && scp ${config[directory]}/*.sms ${config[user]}@${config[ip]}:~/RetroPie/roms/mastersystem/
# Sega Megadrive/Genesis
compgen -G ${config[directory]}/*.md > /dev/null && scp ${config[directory]}/*.md ${config[user]}@${config[ip]}:~/RetroPie/roms/megadrive/
# MAME (pay attention, all zip files will be sent over!)
compgen -G ${config[directory]}/*.zip > /dev/null && scp ${config[directory]}/*.zip ${config[user]}@${config[ip]}:~/RetroPie/roms/mame-libretro/
